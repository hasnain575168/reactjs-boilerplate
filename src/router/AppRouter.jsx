import React from 'react';
import {
  Route,
  Switch,
  BrowserRouter,
} from 'react-router-dom';
import {
  connect,
} from 'react-redux';

const defaultComponent = props => (
  <h1>{props.boilerplate}</h1>
);

const mapStateToProps = state => ({
  boilerplate: state.boilerplate,
});

const connectedDefaultComponent = connect(mapStateToProps)(defaultComponent);

const AppRouter = () => (
  <BrowserRouter>
    <Switch>
      <Route exact to="/" component={connectedDefaultComponent} />
    </Switch>
  </BrowserRouter>
);

export default AppRouter;
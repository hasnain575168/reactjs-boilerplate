import React from 'react';
import {
  Provider,
} from 'react-redux';
import configureStore from '../store/configureStore.jsx';
import AppRouter from '../router/AppRouter.jsx';

const store = configureStore();

const App = () => {
  return (
    <Provider store={store}>
      <AppRouter />
    </Provider>
  );
};

export default App;
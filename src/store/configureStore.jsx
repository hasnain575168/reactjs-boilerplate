import {
  createStore,
  combineReducers,
  compose,
  applyMiddleware,
} from 'redux';
import axios from 'axios';
import ReduxThunk from 'redux-thunk';
import axiosMiddleware from 'redux-axios-middleware';

const defaultReducer = (state = 'ReactJS Boilerplate') => state;

const configureStore = () => {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  const client = axios.create({
    baseUrl: 'http://localhost:3000',
    responseType: 'json',
  });

  const middlewares = [
    // Middlewares i-e redux-thunk,
    ReduxThunk,
    // define payload.request on action to handle requests
    axiosMiddleware(client),
  ];

  const combinedReducers = combineReducers({
    boilerplate: defaultReducer,
  });

  const store = createStore(combinedReducers, composeEnhancers(
    applyMiddleware(...middlewares)
  ));

  return store;
};

export default configureStore;
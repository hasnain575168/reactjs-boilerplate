/* eslint-disable */

const path = require('path');

module.exports = {
  entry: [
    '@babel/polyfill',
    './src/index.jsx'
  ],
  output: {
    path: path.resolve(__dirname, 'dist', 'scripts'),
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.js(x?)$/,
        loader: 'babel-loader',
        exclude: /(node_modules)/,
      },
      {
        test: /\.(s?)css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      }
    ]
  },
  devServer: {
    contentBase: path.resolve(__dirname, 'dist'),
    historyApiFallback: true
  }
};